var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var demo = document.getElementById("demo");
var dctx = demo.getContext("2d");
var rect = canvas.getBoundingClientRect();
var record = new Array();
var step = -1;

var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var x_s,y_s,x,y;

var painting = 0;
var draw_s = 0;
var draw_l = 0;
var draw_c = 0;
var draw_r = 0;
var draw_t = 0;
var moved = 0;
var now = 0;
var tool_l = "p";
var tool_n = "p";

var typing = 0;
var T0 = document.getElementById("t0");
var T1 = document.getElementById("t1");
var T2 = document.getElementById("t2");
var T3 = document.getElementById("t3");
var T4 = document.getElementById("t4");
var T5 = document.getElementById("t5");
var T6 = document.getElementById("t6");
var T7 = document.getElementById("t7");
var T8 = document.getElementById("t8");

var c_value = ["0", "0", "0"];
var c_change = [0, 0, 0];
var c_p;
var color = "rgb("+c_value[0]+","+c_value[1]+","+c_value[2]+")";
document.getElementById("vr").innerHTML = c_value[0];
document.getElementById("vg").innerHTML = c_value[1];
document.getElementById("vb").innerHTML = c_value[2];
dctx.fillStyle = color;
dctx.fillRect(0, 0, demo.width, demo.height);

var solid = document.getElementById("solid");

var size = 1;
var s_change = 0;
document.getElementById("vs").innerHTML = size;

var word = document.getElementById("keyin");
var upbox = document.getElementById("up_b");
var updata = document.getElementById("up_data");
var upbtn = document.getElementById("up_btn");
var xpos = document.getElementById("x_pos");
var ypos = document.getElementById("y_pos");
var picw = document.getElementById("w");
var pich = document.getElementById("h");

rec();

$('#demo').mousedown(function(e)
{
    ctx.beginPath();
    ctx.rect(0, 0, rect.width, rect.height);
    ctx.fillStyle = color;
    ctx.fill();
    rec();
});

$('#canvas').mousedown(function(e)
{
	// Mouse down location
    var mouseX = (e.clientX - rect.left)*canvas.width/rect.width;
    var mouseY = (e.clientY - rect.top)*canvas.height/rect.height;
    x_s = mouseX;
    y_s = mouseY;
    x = mouseX;
    y = mouseY;
	if(tool_n == "p"||tool_n == "e"){
        addClick(mouseX, mouseY, false);
        painting = 1;
        paint();
    }else if(tool_n == "T"){
        typing = 1;
        word.style.left = e.pageX+1+'px';
        word.style.top = e.pageY+1+'px';
        word.style.display = "block";
        word.focus();
    }else if(tool_n == "l"){
        draw_l = 1;
        drawline();
    }else if(tool_n == "c"){
        draw_c = 1;
        drawcir();
    }else if(tool_n == "r"){
        draw_r = 1;
        drawrec();
    }else if(tool_n == "t"){
        draw_t = 1;
        drawtri();
    }
});

$('#canvas').mousemove(function(e){
    var mouseX = (e.clientX - rect.left)*canvas.width/rect.width;
    var mouseY = (e.clientY - rect.top)*canvas.height/rect.height;
    x = mouseX;
    y = mouseY;
    moved = 1;
	if(painting){
        addClick( mouseX, mouseY, true);
        moved = 0;
        paint();
    }else if(draw_l){
        drawline();        
    }else if(draw_c){
        drawcir();        
    }else if(draw_r){
        drawrec();        
    }else if(draw_t){
        drawtri();        
    }
    
});
    
$('#canvas').mouseup(function(e){
    var mouseX = (e.clientX - rect.left)*canvas.width/rect.width;
    var mouseY = (e.clientY - rect.top)*canvas.height/rect.height;
    x = mouseX;
    y = mouseY;
    if(painting){
        paint();
        rec();
        painting = 0;
    }else if(draw_l){
        drawline();  
        rec();
        draw_l = 0;
    }else if(draw_c){
        drawcir();  
        rec();
        draw_c = 0;
    }else if(draw_r){
        drawrec();  
        rec();
        draw_r = 0;
    }else if(draw_t){
        drawtri();  
        rec();
        draw_t = 0;
    }
});
	
$('#canvas').mouseleave(function(e){
    if(painting){
        paint();
        rec();
        painting = 0;
    }else if(draw_l){
        drawline();  
        rec();
        draw_l = 0;
    }else if(draw_c){
        drawcir();  
        rec();
        draw_c = 0;
    }else if(draw_r){
        drawrec();  
        rec();
        draw_r = 0;
    }else if(draw_t){
        drawtri();  
        rec();
        draw_t = 0;
    }
});


$('#red').mousedown(function(e){
    c_change[0] = 1;
});

$('#red').mousemove(function(e){
	if(c_change[0]){
        c_value[0] = document.getElementById("red").value;
		color = "rgb("+c_value[0]+","+c_value[1]+","+c_value[2]+")";
        dctx.fillStyle = color;
        dctx.fillRect(0, 0, demo.width, demo.height);
        document.getElementById("vr").innerHTML = c_value[0];
    }
});
    
$('#red').mouseup(function(e){
    c_change[0] = 0;
    c_value[0] = document.getElementById("red").value;
    color = "rgb("+c_value[0]+","+c_value[1]+","+c_value[2]+")";
    dctx.fillStyle = color;
    dctx.fillRect(0, 0, demo.width, demo.height);
    document.getElementById("vr").innerHTML = c_value[0];
});

$('#green').mousedown(function(e){
	c_change[1] = 1;
});

$('#green').mousemove(function(e){
	if(c_change[1]){
        c_value[1] = document.getElementById("green").value;
		color = "rgb("+c_value[0]+","+c_value[1]+","+c_value[2]+")";
        dctx.fillStyle = color;
        dctx.fillRect(0, 0, demo.width, demo.height);
        document.getElementById("vg").innerHTML = c_value[1];
    }
});
    
$('#green').mouseup(function(e){
    c_change[1] = 0;
    c_value[1] = document.getElementById("green").value;
    color = "rgb("+c_value[0]+","+c_value[1]+","+c_value[2]+")";
    dctx.fillStyle = color;
    dctx.fillRect(0, 0, demo.width, demo.height);
    document.getElementById("vg").innerHTML = c_value[1];
});

$('#blue').mousedown(function(e){
	c_change[2] = 1;
});

$('#blue').mousemove(function(e){
	if(c_change[2]){
        c_value[2] = document.getElementById("blue").value;
		color = "rgb("+c_value[0]+","+c_value[1]+","+c_value[2]+")";
        dctx.fillStyle = color;
        dctx.fillRect(0, 0, demo.width, demo.height);
        document.getElementById("vb").innerHTML = c_value[2];
    }
});
    
$('#blue').mouseup(function(e){
    c_change[2] = 0;
    c_value[2] = document.getElementById("blue").value;
	color = "rgb("+c_value[0]+","+c_value[1]+","+c_value[2]+")";
    dctx.fillStyle = color;
    dctx.fillRect(0, 0, demo.width, demo.height);
    document.getElementById("vb").innerHTML = c_value[2];
});

$('#size').mousedown(function(e){
	s_change = 1;
});

$('#size').mousemove(function(e){
	if(s_change){
        size = document.getElementById("size").value;
        document.getElementById("vs").innerHTML = size;
    }
});
    
$('#size').mouseup(function(e){
    s_change = 0;
    size = document.getElementById("size").value;
    document.getElementById("vs").innerHTML = size;
});

$('#save').click(function(){
    var _url = canvas.toDataURL();
    this.href = _url;
});

$('#keyin').keypress(function(e) {
    if(e.which=='13') {
        drawtext();
        if(word.value != "") rec();
        typing = 0;
        word.style.display = "none";
        word.value = "";
    }
});

function addClick(x, y, dragging){
    clickX.push(x);
    clickY.push(y);
    clickDrag.push(dragging);
}

function paint(){
  ctx.lineJoin = "round";
  ctx.lineWidth = size/2;	
  for(;now < clickX.length; now++) {		
    ctx.beginPath();
    if(clickDrag[now] && now){
        ctx.moveTo(clickX[now-1], clickY[now-1]);
    }else{
        ctx.moveTo(clickX[now]-1, clickY[now]);
    }
    ctx.lineTo(clickX[now], clickY[now]);
    ctx.closePath();
    if(tool_n=='p') ctx.strokeStyle = color;
    else if(tool_n=='e') ctx.strokeStyle = "white";
    ctx.stroke();
  }
}

function drawtext(){
    var typeface;
    if(T0.selected) typeface = "serif";
    else if(T1.selected) typeface = "sans-serif";
    else if(T2.selected) typeface = "cursive";
    else if(T3.selected) typeface = "fantasy";
    else if(T4.selected) typeface = "monospace";
    else if(T5.selected) typeface = "MingLiU";
    else if(T6.selected) typeface = "PMingLiU";
    else if(T7.selected) typeface = "DFKai-sb";
    else if(T8.selected) typeface = "Microsoft JhengHei";

    ctx.font = (Number(size)+20) + "px " + typeface;
    if(solid.checked){
        ctx.fillStyle = color;
        ctx.fillText(word.value, x_s, y_s, ctx.measureText(word.value).width);
    }else{
        ctx.strokeStyle = color;
        ctx.strokeText(word.value, x_s, y_s, ctx.measureText(word.value).width);
    }
}

function drawline(){
    ctx.lineJoin = "round";
    ctx.lineCap = "round";
    ctx.lineWidth = size/2;
    ctx.strokeStyle = color;
    if(moved){
        var canvasPic = new Image();
        canvasPic.src = record[step];
        canvasPic.onload = function () { 
            canvas.height = canvas.height;
            ctx.drawImage(canvasPic, 0, 0);
        }
        moved = 0;
    }
    ctx.moveTo(x_s,y_s);
    ctx.lineTo(x,y);
    ctx.stroke();
}

function drawcir(){
    var r = (x-x_s)*(x-x_s) + (y-y_s)*(y-y_s);
    ctx.lineJoin = "round";
    ctx.lineWidth = size/2;
    ctx.strokeStyle = color;
    if(moved){
        var canvasPic = new Image();
        canvasPic.src = record[step];
        canvasPic.onload = function () { 
            canvas.height = canvas.height;
            ctx.drawImage(canvasPic, 0, 0);
        }
        moved = 0;
    }
    ctx.beginPath();
    ctx.arc(x_s, y_s, Math.sqrt(r), 0, 2 * Math.PI);
    if(solid.checked){
        ctx.fillStyle = color;
        ctx.fill();
    }
    ctx.stroke();
}

function drawrec(){
    var x0 = (x>x_s) ? x_s: x;
    var y0 = (y>y_s) ? y_s: y;
    ctx.lineJoin = "round";
    ctx.lineWidth = size/2;
    ctx.strokeStyle = color;
    if(moved){
        var canvasPic = new Image();
        canvasPic.src = record[step];
        canvasPic.onload = function () { 
            canvas.height = canvas.height;
            ctx.drawImage(canvasPic, 0, 0);
        }
        moved = 0;
    }
    ctx.beginPath();
    ctx.rect(x0, y0, Math.abs(x-x_s), Math.abs(y-y_s));
    if(solid.checked){
        ctx.fillStyle = color;
        ctx.fill();
    }
    ctx.stroke();
}

function drawtri(){
    ctx.lineJoin = "round";
    ctx.lineWidth = size/2;
    ctx.strokeStyle = color;
    if(moved){
        var canvasPic = new Image();
        canvasPic.src = record[step];
        canvasPic.onload = function () { 
            canvas.height = canvas.height;
            ctx.drawImage(canvasPic, 0, 0);
        }
        moved = 0;
    }
    ctx.beginPath();
    ctx.moveTo(x_s,y_s);
    ctx.lineTo(x,y);
    ctx.lineTo(2*x_s-x,y);
    ctx.closePath();
    if(solid.checked){
        ctx.fillStyle = color;
        ctx.fill();
    }
    ctx.stroke();
}

function change(i){
    if(i=="x") i = tool_n;
    tool_l = tool_n;
    tool_n = i;
    document.getElementById(tool_l).setAttribute("class","btn");
    document.getElementById(tool_n).setAttribute("class","btn2");
    word.style.display = "none";
    word.value = "";

    if(tool_n == "p") canvas.style.cursor = 'url("paint-brush.png"),auto';
    else if(tool_n == "e") canvas.style.cursor = 'url("eraser.png"),auto';
    else if(tool_n == "T") canvas.style.cursor = "text";
    else if(tool_n == "l") canvas.style.cursor = 'url("line.png"),auto';
    else if(tool_n == "c" && solid.checked) canvas.style.cursor = 'url("circle_s.png"),auto';
    else if(tool_n == "c") canvas.style.cursor = 'url("circle.png"),auto';
    else if(tool_n == "r" && solid.checked) canvas.style.cursor = 'url("rectangle_s.png"),auto';
    else if(tool_n == "r") canvas.style.cursor = 'url("rectangle.png"),auto';
    else if(tool_n == "t" && solid.checked) canvas.style.cursor = 'url("triangle_s.png"),auto';
    else if(tool_n == "t") canvas.style.cursor = 'url("triangle.png"),auto';
    else canvas.style.cursor = "default";
}

function rec() {
    step++;
    if (step < record.length) { record.length = step; }
    record.push(canvas.toDataURL());
}  

function undo() {
    if (step > 0) {
        step--;
        var canvasPic = new Image();
        canvasPic.src = record[step];
        canvasPic.onload = function () { 
            canvas.height = canvas.height;
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
} 

function redo() {
    if (step < record.length-1) {
        step++;
        var canvasPic = new Image();
        canvasPic.src = record[step];
        canvasPic.onload = function () {
            canvas.height = canvas.height;
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
}  
var count=0;

function upload() {
    upbox.style.display = "block";
    var pic = new Image();
    var preview = new Image();
    var draw = 1;
    var reader  = new FileReader();
    var pre = document.getElementById("Preview");
    $('#myFile').change(function(e) {
        file  = document.getElementById("myFile").files[0];
        pic.src = "";
        reader.addEventListener("load", function () {
            pic.src = reader.result;
            preview.src = reader.result;
            updata.style.display = "block";
            while(pre.firstChild){
                pre.removeChild(pre.firstChild);
            }
            preview.height = 100;
            document.getElementById("Preview").appendChild(preview);
            $('#drawpic').click(function(){ 
                if(xpos.value!=""&&ypos.value!=""){
                    if(draw){
                        if(pich.value==""||picw.value=="") ctx.drawImage(pic, xpos.value, ypos.value);
                        else ctx.drawImage(pic, xpos.value, ypos.value, picw.value, pich.value);
                        rec();
                    }
                    draw = 0;
                    upbox.style.display = "none";
                    document.getElementById("myFile").value = "";
                }
            });
        }, false);        
        if (file) {
            reader.readAsDataURL(file);
        }
    });
} 

function closeup() {
    upbox.style.display = "none";
} 

function clr() {
    canvas.height = canvas.height;
    rec();
}