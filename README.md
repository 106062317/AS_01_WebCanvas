# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
<img src="intro.png" width="640px" height="260px"></img>
* Brush and eraser: 在畫布上移動以畫出或清除筆跡。
* Color selector: 拖動r.g.b旁的slider可分別在0~255範圍中調整，slider右方會顯示目前數值。
    * 上方的當前色彩展示區會展是目前顏色。
    * 顏色會套用至畫筆、文字、線段、實心/空心圖形。
* Simple menu: 藉由size可調整筆畫粗細。
* Text input: 點選畫布會在該位置出現輸入框，按下enter後會依當前font和size設定字型字體。
* Font menu: 字型藉由下拉選單調整，字體藉由size調整。
* Cursor icon: 更換工具或改變是否實心時，鼠標圖形會一起更動。
    * 畫筆、橡皮擦、文字、線段、實心/空心圓、實心/空心矩形、實心/空心三角形。
* Refresh button: 按下後重置畫布。
* Different brush shapes: 直線、圓形、矩形、三角形。
    * 按下並拖曳畫出圖形。
    * 根據solid調整是否為實心圖形。
* Un/Re-do button: 按下可復原與重做對畫布的操作。
    * 也包含重置與更改畫布顏色。
* Image tool: 點選上傳後，會在左方出現方塊，可選擇檔案。
    * 選好檔案可在下方預覽，可輸入想放的位置與想調整的長寬
    * 未輸入長寬將以原尺寸貼上圖片
* Download: 點選後下載當前畫布。
    * 檔案為my_painting.png
* 補充:
    * Size:可調整字體大小、筆畫粗細(畫筆、橡皮擦、線段、空心圖形)。
    * Solid:可調整圖形與文字是否實心，並改變對應狀況鼠標。
    * Filled canvas:點選當前色彩展示區，可將整個畫布改為該顏色。
